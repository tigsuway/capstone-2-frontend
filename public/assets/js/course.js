let params = new URLSearchParams(window.location.search);

let cid = params.get('courseId');
let uid = localStorage.getItem('id');

let name = document.querySelector('#courseName');
let desc = document.querySelector('#courseDesc');
let price = document.querySelector('#coursePrice');
let enrollDiv = document.querySelector('#enrollDiv');
let enrollButton = document.querySelector('#enrollButton');

// If user is not logged in or is an Admin do not display enroll.

if (!localStorage.getItem('token')) {
	enrollDiv.innerHTML = '';
} else {
	if (
		localStorage.getItem('isAdmin') === 'false' ||
		localStorage.getItem('isAdmin') === null
	) {
		fetch(
			`https://floating-river-16143.herokuapp.com/api/users/${uid}/course/${cid}`
		)
			.then((res) => res.json())
			.then((enrolled) => {
				if (enrolled.isEnrolled === false) {
					enrollButton.removeAttribute('disabled');
				} else {
					enrollButton.setAttribute('disabled', 'true');
				}
			});
	} else {
		enrollButton.setAttribute('disabled', 'true');
	}
}

enrollButton.addEventListener('click', (event) => {
	event.preventDefault();
	Swal.fire({
		title: 'Are you sure do you want to enroll?',
		showDenyButton: false,
		showCancelButton: true,
		confirmButtonText: `Enroll`,
	}).then((result) => {
		/* Read more about isConfirmed, isDenied below */
		if (result.isConfirmed) {
			fetch(
				'https://floating-river-16143.herokuapp.com/api/users/enroll',
				{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem(
							'token'
						)}`,
					},
					body: JSON.stringify({
						courseId: cid,
					}),
				}
			)
				.then((res) => res.json())
				.then((data) => {
					if (data === true) {
						Swal.fire({
							icon: 'success',
							title: 'Enrollment Successful!',
							showDenyButton: false,
							showCancelButton: false,
							confirmButtonText: `OK`,
						}).then((result) => {
							/* Read more about isConfirmed, isDenied below */
							if (result.isConfirmed) {
								window.location.replace(
									`./course.html?courseId=${cid}`
								);
							}
						});
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Ooops! Something went wrong...',
							showDenyButton: false,
							showCancelButton: false,
							confirmButtonText: `OK`,
						}).then((result) => {
							/* Read more about isConfirmed, isDenied below */
							if (result.isConfirmed) {
								window.location.replace(
									`./course.html?courseId=${cid}`
								);
							}
						});
						Swal.fire({
							icon: 'error',
							title: 'Enrollment Failed!',
							text:
								'Oppps! An issue occurred while enrolling, please contact the administrator.',
						});
					}
				});
		}
	});
});

fetch(`https://floating-river-16143.herokuapp.com/api/courses/${cid}`)
	.then((res) => res.json())
	.then((data) => {
		name.innerHTML = data.name;
		price.innerHTML = '$' + data.price;
		desc.innerHTML = data.description;
	});
