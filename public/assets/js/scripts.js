// Capture the navSession element
let navItem = document.querySelector('#navSession');
let admin = document.querySelector('#admin');
let profile = document.querySelector('#profile');
let myCourse = document.querySelector('#myCourse');

// Takes the access token from the local storage property.
let userToken = localStorage.getItem('token');

isAdmin = localStorage.getItem('isAdmin');

if (!userToken) {
	profile.innerHTML = '';
	myCourse.innerHTML = '';
	navItem.innerHTML = `
    <a href="./login.html" style="color: white"> Login </a>
  `;
} else {
	navItem.innerHTML = `
    <a href="./logout.html" style="color: white"> Logout </a>
  `;
	if (isAdmin == 'true') {
		admin.innerHTML = `
      <a href="#">Administration</a>
      <ul>
        <li><a href="./addCourse.html">Add Course</a></li>
        <li><a href="./register.html">Add User</a></li>
      </ul>
    `;
		profile.innerHTML = '';
		myCourse.innerHTML = '';
	} else {
		admin.innerHTML = '';
	}
}
