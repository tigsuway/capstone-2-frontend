let modalButton = document.querySelector('#addCourse');
let container = document.querySelector('#coursesContainer');
let logout = document.querySelector('#navSession');
let courseContainer = `
<div class="container" data-aos="fade-up">
  <div id="coursesRow" class="row" data-aos="zoom-in" data-aos-delay="100">
  </div>
</div>
`;
let isAdmin = localStorage.getItem('isAdmin');
let coursesRow = '';
let cardFooter;
let cardBg = '';
let params = new URLSearchParams(window.location.search);
let cid = params.get('courseId');
let token = localStorage.getItem('token');

console.log(cid);

if (isAdmin === 'false' || isAdmin === 'undefined') {
	//if a user is a regular user, do not show the addcourse button.
	modalButton.innerHTML = 'null';
} else {
	modalButton.innerHTML = `
    <div class="col-md-4 offset-md-4"></div>
      <div class="col-md-4 offset-md-4" style="text-align: center"><a href="./addCourse.html" class="btn btn-block btn-success">Add Course</a>
      </div>
    <div class="col-md-4 offset-md-4"></div>
   `;
}

fetch('https://floating-river-16143.herokuapp.com/api/courses/')
	.then((res) => res.json())
	.then((data) => {
		let courseData;

		if (data.length < 1) {
			courseData = 'No Course Available';
		} else {
			container.innerHTML = courseContainer;
			coursesRow = document.querySelector('#coursesRow');
			// console.log('DATA:' + JSON.stringify(data));
			courseData = data
				.map((course) => {
					if (isAdmin === 'true') {
						if (!course.isActive) {
							cardFooter = `
                <a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
                <a href="#" class="btn btn-info text-white btn-block"> Edit </a>
                <a href="./courses.html?courseId=${course._id}" class="btn btn-warning text-white btn-block"> Enable Course </a>
              `;
						} else {
							cardFooter = `
                <a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
                <a href="#" class="btn btn-info text-white btn-block"> Edit </a>
                <a href="./courses.html?courseId=${course._id}" class="btn btn-danger text-white btn-block"> Disable Course </a>
              `;
						}
					} else {
						cardFooter = `
              <a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
            `;
					}
					return `
            <div class="col-lg-4 col-md-6 d-flex align-items-stretch" style="margin-bottom: 50px;">
              <div class="course-item">
                <img
                  src="../assets/img/course-5.jpg"
                  class="img-fluid"
                  alt="..."
                />
                <div class="course-content">
                  <div
                    class="d-flex justify-content-between align-items-center mb-3"
                  >
                    <h4 class="courseButton">${course.name}</h4>
                    <p class="price">$${course.price}</p>
                  </div>
                  <p>
                    ${course.description}
                  </p>
                  <div class="card-footer" style="margin: 0; padding: 0;">
                    ${cardFooter}
                  </div>
                </div>
              </div>
            </div>
          `;
				})
				.join(''); // We used join method to create  a return of new string
			// It concatenated all the objects inside the array and converted each to a string data type
		}
		coursesRow.innerHTML = courseData;
	});

if (cid !== null) {
	fetch(`https://floating-river-16143.herokuapp.com/api/courses/${cid}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`,
		},
	})
		.then((res) => res.json())
		.then((data) => {
			console.log(data);
			if (data.isActive === true) {
				Swal.fire({
					icon: 'success',
					title: 'Course Activated!',
					text: `Course "${data.name}" can now be seen by students.`,
				}).then(() => {
					window.location.replace('./courses.html');
				});
			} else if (data.isActive === false) {
				Swal.fire({
					icon: 'warning',
					title: 'Course Deactivated!',
					text: `Course "${data.name}" is now hidden from students.`,
				}).then(() => {
					window.location.replace('./courses.html');
				});
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: 'Something went wrong!',
				}).then(() => {
					window.location.replace('./courses.html');
				});
			}
		});
}
