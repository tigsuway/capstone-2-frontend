//get the access token in the localStorage
let token = localStorage.getItem('token');

let profileForm = document.querySelector('#profileForm');

//lets create a control structure that will determin the display if the access token is null or empty.
if (!token || token === null) {
	//lets redirect the user to the login page
	alert('You must Login First');
	window.location.href = './login.html';
} else {
	fetch('https://floating-river-16143.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`,
		},
	})
		.then((res) => res.json())
		.then((data) => {
			profileForm.innerHTML = `
      <div class="form-group">
        <input
          type="text"
          name="firstName"
          class="form-control"
          id="firstName"
          value="${data.firstName}"
          readonly
        />
      </div>
      <div class="form-group">
        <input
          type="text"
          name="lastName"
          class="form-control"
          id="lastName"
          value="${data.lastName}"
          readonly
        />
      </div>
      <div class="form-group">
        <input
          type="email"
          name="userEmail"
          class="form-control"
          id="userEmail"
          value="${data.email}"
          readonly
        />
      </div>
      <div class="form-group">
        <input
          type="tel"
          name="mobileNo"
          class="form-control"
          id="mobileNumber"
          value="${data.mobileNo}"
          readonly
        />
      </div>
      <div class="form-group">
        <button type="submit" class="update-profile-btn">
          Update Information
        </button>
      </div>  
    </div>
    `;
		});
}
