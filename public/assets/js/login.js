let loginForm = document.querySelector('#loginUser');

// console.log(localStorage.getItem("token"));
if (localStorage.getItem('id') !== null) {
	window.location.replace('./courses.html');
}

loginForm.addEventListener('submit', (event) => {
	event.preventDefault();

	let username = document.querySelector('#userEmail').value;
	let password = document.querySelector('#password').value;
	if (username !== '' || password !== '') {
		fetch('https://floating-river-16143.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: username,
				password: password,
			}),
			// redirect: "follow",
		})
			.then((res) => res.json())
			.then((data) => {
				if (data) {
					if (data.access) {
						// save the access token inside our local storage
						localStorage.setItem('token', data.access);
						// alert("Access key save on local storage.");
						fetch(
							'https://floating-river-16143.herokuapp.com/api/users/details',
							{
								headers: {
									Authorization: `Bearer ${data.access}`,
								},
							}
						)
							.then((res) => {
								return res.json();
							})
							.then((data) => {
								console.log(data);
								console.log(
									'--' + data._id + '--' + data.isAdmin + '--'
								);
								localStorage.setItem('id', data._id);
								localStorage.setItem('isAdmin', data.isAdmin);
								// console.log("Items are set inside the local storage.");
								// console.log(localStorage.getItem("isAdmin"));
								return (window.location.href =
									'./courses.html');
							});
					} else {
						// no existing access key value from the data variable then just inform the user
						Swal.fire({
							icon: 'warning',
							title: 'Warning',
							text: "Username and password doesn't match!",
						});
					}
				} else {
					return Swal.fire({
						icon: 'warning',
						title: 'Warning',
						text: "Username and password doesn't match!",
					});
				}
			});
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Failed!',
			text: 'Username and/or password cannot be empty!',
		});
	}
});
