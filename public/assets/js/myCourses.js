// alert("hello");
let modalButton = document.querySelector('#addCourse');
let container = document.querySelector('#myCoursesContainer');
let logout = document.querySelector('#navSession');
let courseContainer = `
<div class="container" data-aos="fade-up">
  <div id="coursesRow" class="row" data-aos="zoom-in" data-aos-delay="100">
  </div>
</div>
`;
let isAdmin = localStorage.getItem('isAdmin');
let id = localStorage.getItem('id');
let coursesRow = '';
let cardFooter;
let enrolledCourses = [];
let myCourses = [];
let courseStatus;

const compare = (a, b) => {
	const nameA = a.name.toUpperCase();
	const nameB = b.name.toUpperCase();

	let comparison = 0;
	if (nameA > nameB) {
		comparison = 1;
	} else if (nameA < nameB) {
		comparison = -1;
	}
	return comparison;
};

// console.log('123: ' + id);

if (isAdmin == 'false' || !isAdmin) {
	//if a user is a regular user, do not show the addcourse button.
	// modalButton.innerHTML = "";
} else {
	modalButton.innerHTML = `
    <div class="col-md-4 offset-md-4"></div>
      <div class="col-md-4 offset-md-4" style="text-align: center"><a href="./addCourse.html" class="btn btn-block btn-success">Add Course</a>
      </div>
    <div class="col-md-4 offset-md-4"></div>
   `;
}

fetch(`https://floating-river-16143.herokuapp.com/api/users/my-courses`, {
	headers: {
		'Content-Type': 'application/json',
		Authorization: `Bearer ${localStorage.getItem('token')}`,
	},
})
	.then((res) => res.json())
	.then((data) => {
		if (data.length < 1) {
			courseData = '<h3>You are not enrolled to any course.</h3>';
		} else {
			console.log('Dataaa: ' + JSON.stringify(data));
			enrolledCourses = data;

			container.innerHTML = courseContainer;
			coursesRow = document.querySelector('#coursesRow');
			console.log('E: ' + JSON.stringify(enrolledCourses));
			for (let i = 0; i < enrolledCourses.length; i++) {
				console.log(`IDs: ${enrolledCourses[i].courseId}`);
				fetch(
					`https://floating-river-16143.herokuapp.com/api/courses/${enrolledCourses[i].courseId}`
				)
					.then((res) => res.json())
					.then((course) => {
						console.log(`Course name: ${course.name}`);
						myCourses.push(course);
						console.log('index:' + i);
						if (i === enrolledCourses.length - 1) {
							console.log('Returned!');
							return myCourses.sort();
						}
					})
					.then(() => {
						let tempArr = myCourses.sort(compare);
						return (myCourses = tempArr);
					})
					.then(() => {
						console.log(`My Courses: ${JSON.stringify(myCourses)}`);
						courseData = myCourses
							.map((course) => {
								courseStatus = 'Inactive';
								if (course.isActive) courseStatus = 'Active';
								return `
									<div class="col-lg-4 col-md-6 d-flex align-items-stretch" style="margin-bottom: 50px">
										<div class="course-item">
											<img
												src="../assets/img/course-5.jpg"
												class="img-fluid"
												alt="..."
											/>
											<div class="course-content">
												<!-- <h7>Course ID: ${course._id}</h7> -->
												<div
													class="d-flex justify-content-between align-items-center mb-3"
												>
													<h4 class="courseButton">${course.name}</h4>
													<p class="price">${courseStatus}</p>
												</div>
												<p>
												${course.description}
												</p>
											</div>
										</div>
									</div>
								`;
							})
							.join('');
						coursesRow.innerHTML = courseData;
					});
			}
		}
	});
