let createCourseForm = document.querySelector('#createCourse');

createCourseForm.addEventListener('submit', (event) => {
	event.preventDefault();

	let courseName = document.querySelector('#courseName').value;
	let coursePrice = document.querySelector('#coursePrice').value;
	let courseDescription = document.querySelector('#courseDescription').value;
	// let courseImage = document.querySelector("#courseImage").name;

	if (courseName !== '' && courseDescription !== '' && coursePrice !== '') {
		fetch(
			'https://floating-river-16143.herokuapp.com/api/courses/course-exists',
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					name: courseName,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data === false) {
					fetch(
						'https://floating-river-16143.herokuapp.com/api/courses/addCourse',
						{
							method: 'POST',
							headers: {
								'Content-Type': 'application/json',
							},
							body: JSON.stringify({
								name: courseName,
								price: coursePrice,
								description: courseDescription,
								// imageFile: courseImage,
							}),
						}
					)
						.then((res) => {
							return res.json();
						})
						.then((data) => {
							console.log(data);
							if (data === true) {
								return Swal.fire({
									icon: 'success',
									title: 'Success!',
									text: 'Course was successfully created!',
								});
							} else {
								return Swal.fire({
									icon: 'error',
									title: 'Failed!',
									text:
										'Something went wrong with your course creation!',
								});
							}
						});
				} else {
					Swal.fire({
						icon: 'warning',
						title: 'Warning',
						text: 'Course name exists!',
					});
				}
			});
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Failed!',
			text: 'Something went wrong with your course creation!',
		});
	}
});
