let registerForm = document.querySelector('#registerUser');

registerForm.addEventListener('submit', (event) => {
	event.preventDefault();

	let firstName = document.querySelector('#firstName').value;
	// console.log(firstName);
	let lastName = document.querySelector('#lastName').value;
	// console.log(lastName);
	let userEmail = document.querySelector('#userEmail').value;
	// console.log(userEmail);
	let mobileNumber = document.querySelector('#mobileNumber').value;
	// console.log(mobileNumber);
	let password = document.querySelector('#password').value;
	// console.log(password);
	let verifyPassword = document.querySelector('#confirm').value;
	// console.log(verifyPassword);

	if (
		password !== '' &&
		verifyPassword !== '' &&
		password === verifyPassword &&
		mobileNumber.length === 11
	) {
		fetch(
			'https://floating-river-16143.herokuapp.com/api/users/email-exists',
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					email: userEmail,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data === false) {
					fetch(
						'https://floating-river-16143.herokuapp.com/api/users/register',
						{
							method: 'POST',
							headers: {
								'Content-Type': 'application/json',
							},
							body: JSON.stringify({
								firstName: firstName,
								lastName: lastName,
								email: userEmail,
								mobileNo: mobileNumber,
								password: password,
							}),
						}
					)
						.then((res) => {
							return res.json();
						})
						.then((data) => {
							console.log(data);
							if (data === true) {
								Swal.fire({
									icon: 'success',
									title: 'Success!',
									text: 'User was successfully registered!',
								});
								// firstName = "";
								// astName = "";
								// userEmail = "";
								// mobileNumber = "";
								// password = "";
								// verifyPassword = "";
							} else {
								Swal.fire({
									icon: 'error',
									title: 'Failed!',
									text:
										'Something went wrong in the registration!',
								});
							}
						});
				} else {
					Swal.fire({
						icon: 'warning',
						title: 'Exists!',
						text: 'Email already exists!',
					});
				}
			});
	} else {
		Swal.fire({
			icon: 'error',
			title: 'Failed!',
			text: 'Something went wrong in the registration!',
		});
	}
});
